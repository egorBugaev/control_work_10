import {FETCH_GET_SUCCESS, FETCH_POST_SUCCESS, FETCH_POST_ERROR, FETCH_POST_SUCCESS_COMMENT, FETCH_DELETE_SUCCESS, FETCH_GET_SUCCESS_NEWS,FETCH_DELETE_SUCCESS_COMMENT} from "./actionTypes";

const initialState = {
	news: [],
	error: '',
	newsOne: {}
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_POST_SUCCESS:
			const news = [...state.news];
			news.push(action.data);
			return {...state, news, error: ''};
		case FETCH_POST_SUCCESS_COMMENT:
			const newsOneComments = [...state.newsOne];
			newsOneComments.push(action.data);
			return {...state, newsOne: newsOneComments, error: ''};
		case FETCH_POST_ERROR:
			return {...state, error: action.error.response.data.sqlMessage};
		case FETCH_GET_SUCCESS:
			return {...state, news: action.news, error: ''};
		case FETCH_GET_SUCCESS_NEWS:
			return {...state, newsOne: action.newsOne[0], error: ''};
		case FETCH_DELETE_SUCCESS:
			const newsAll = [...state.news];
			const index = newsAll.findIndex(p => p.id === action.id);
			newsAll.splice(index,1);
			return {...state, news: newsAll, error: ''};
		case FETCH_DELETE_SUCCESS_COMMENT:
			const newsOne = {...state.newsOne};
			console.log(newsOne);
			const id = newsOne.comments.findIndex(p => p.id === action.id);
			newsOne.comments.splice(id,1);
			return {...state, newsOne: newsOne, error: ''};
		default:
			return state;
	}
};

export default reducer;
