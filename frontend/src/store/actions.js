import {FETCH_GET_SUCCESS, FETCH_POST_SUCCESS, FETCH_POST_ERROR, FETCH_POST_SUCCESS_COMMENT, FETCH_DELETE_SUCCESS, FETCH_GET_SUCCESS_NEWS,FETCH_DELETE_SUCCESS_COMMENT} from "./actionTypes";
import axios from '../axios-api';

export const fetchPostSuccess = data => {
	return {type: FETCH_POST_SUCCESS, data};
};

export const fetchPostSuccessComment = data => {
	return {type: FETCH_POST_SUCCESS_COMMENT, data};
};

export const fetchGetSuccess = news => {
	return {type: FETCH_GET_SUCCESS, news};
};
export const fetchGetSuccessNews = newsOne => {
	return {type: FETCH_GET_SUCCESS_NEWS, newsOne};
};

export const fetchPostError = error => {
	return {type: FETCH_POST_ERROR, error}
};

export const fetchDeleteSuccess = id => {
	return {type: FETCH_DELETE_SUCCESS, id}
};
export const fetchDeleteSuccessComment = id => {
	return {type: FETCH_DELETE_SUCCESS_COMMENT, id}
};

export const postFormToServerDb = data => {
	return dispatch => {
		return axios.post('/news', data).then(
			response => dispatch(fetchPostSuccess(response.data))
		).catch(error => dispatch(fetchPostError(error)));
	}
};
export const postFormToServerDbComment = data => {
	return dispatch => {
		return axios.post('/comments', data).then(
			response => dispatch(fetchPostSuccess(response.data))
		).catch(error => dispatch(fetchPostError(error)));
	}
};

export const getNews = () => {
	return dispatch => {
		return axios.get('/news').then(
			response => dispatch(fetchGetSuccess(response.data))
		);
	};
};

export const getNewsByID = id => {
	return dispatch => {
		return axios.get(`/news/${id}`).then(response => dispatch(fetchGetSuccessNews(response.data)));
	}
};

export const deleteNews = id => {
	return dispatch => {
		return axios.delete(`/news/${id}`).then(
			response => dispatch(fetchDeleteSuccess(id))
		)
	}
};
export const deleteComment = id => {
	return dispatch => {
		return axios.delete(`/comments/${id}`).then(
			response => dispatch(fetchDeleteSuccessComment(id))
		)
	}
};
