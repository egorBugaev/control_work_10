import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';

const Header = () => {
	return (
		<header>
			<NavLink exact to="/">News List</NavLink>
			<NavLink exact to="/add_edit">Add News</NavLink>
		</header>
	)
};

export default Header;