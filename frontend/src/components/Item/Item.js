import React from 'react';
import './item.css';
import AddComment from '../../containers/news/addComment'

const Item = props => {
	let id = props.item.id;
	let list =null;
	if(props.item.comments){
	list= <ul>
		{
			props.item.comments.map(item => {
				return <li
					key={item.id}
					className="this-item">
					<h3>{item.author}</h3>
					<p>{item.comment}</p>
					<button className="delete" onClick={() => props.delete(item.id)}>Удалить</button>
				</li>
			})
		}
	</ul>
	}
	return (
		<div className="item">
			<h2>{props.item.name}</h2>
			<p>{props.item.content}</p>
			<p>{props.item.date}</p>
			{
				(props.item.image && props.item.image !== 'null') && <img src={'http://localhost:8000/uploads/' + props.item.image} alt=""/>
			}
			{list}
			<AddComment id={id}/>
		</div>
	)
};

export default Item;