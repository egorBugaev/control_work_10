import React, {Component} from 'react';
import {connect} from "react-redux";
import {postFormToServerDbComment} from "../../store/actions";

class AddComment extends Component {
	
	state = {
		author: '',
		comment: '',
		news_id:''
	};
	
	componentDidMount(){
  let id = this.props.id;
		console.log(id);
		this.setState({news_id:id})
	}
	
	sendForm = event => {
		event.preventDefault();
		this.props.sendFormToServer(this.state).then(response => {
			if(!response.error) {
				this.setState({
					author: '',
					comment: '',
					id:''
				});
			}
		});
	};
	
	inputChangeHandler = event => {
		event.persist();
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	
	render() {
		return (
			<div className="add-edit-item">
				<h2>Новость</h2>
				<form onSubmit={this.sendForm}>
					<div className="row">
						<label htmlFor="name">Автор</label>
						<input value={this.state.author} onChange={this.inputChangeHandler} id="author" name="author" type="text"/>
					</div>
					<div className="row">
						<label htmlFor="description">Комент</label>
						<textarea value={this.state.comment} required onChange={this.inputChangeHandler} id="comment" name="comment" type="text"/>
					</div>
					<button>Отправить</button>
				</form>
				<span className="error">{this.props.error}</span>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		error: state.error
	};
};

const mapDispatchToProps = dispatch => {
	return {
		sendFormToServer: (data) => dispatch(postFormToServerDbComment(data))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(AddComment);
