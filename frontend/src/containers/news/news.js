import React, {Component,} from 'react';

import {connect} from "react-redux";
import {deleteComment, getNews, getNewsByID} from "../../store/actions";
import Item from "../../components/Item/Item";

class NewsOne extends Component {
	
	render() {
		return (
			<Item
			item={this.props.newsOne}
			delete={(id) =>this.props.deleteCommentFromDB(id)}
			/>
		
		)
	}
}

const mapStateToProps = state => {
	return {
		newsOne: state.newsOne,
		error: state.error
	};
};

const mapDispatchToProps = dispatch => {
	return {
		getAllNews: () => dispatch(getNews()),
		getNewsByID: (id) => dispatch(getNewsByID(id)),
		deleteCommentFromDB: (id) => dispatch(deleteComment(id))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsOne);