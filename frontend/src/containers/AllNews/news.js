import React, {Component, Fragment} from 'react';
import './Items.css';
import {connect} from "react-redux";
import {deleteNews, getNews, getNewsByID} from "../../store/actions";

class News extends Component {
	
	
	componentDidMount() {
		this.props.getAllNews();
	};
	

	
	getNews = id => {
		this.props.history.push('/news/'+id);
		this.props.getNewsByID(id)
	};
	
	deleteItem = id => {
		this.props.deleteNewsFromDB(id);
	};
	
	render() {
		return (
			<Fragment>
				<div className="items">
					<ul>
						{
						
							this.props.news.map(item => {
								return <li
									key={item.id}
									className="this-item">
									<h3>{item.name}</h3>
									<p>{item.date}</p>
									{item.image && <img src={'http://localhost:8000/uploads/'+item.image} alt={'alt'}/>}
									<button className="delete" onClick={() => this.deleteItem(item.id)}>Удалить</button>
									<button className="delete" onClick={() => this.getNews(item.id)}>Подробнее</button>
									</li>
							})
						}
					</ul>
				</div>
			</Fragment>
		
		)
	}
}

const mapStateToProps = state => {
	return {
		news: state.news,
		error: state.error
	};
};

const mapDispatchToProps = dispatch => {
	return {
		getAllNews: () => dispatch(getNews()),
		getNewsByID: (id) => dispatch(getNewsByID(id)),
		deleteNewsFromDB: (id) => dispatch(deleteNews(id))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(News);