import React, {Component, Fragment} from 'react';
import Header from "./components/Header/Header";
import News from "./containers/AllNews/news";
import Add from "./containers/AddEdit/Add";
import NewsOne from "./containers/news/news"
import {Route, Switch} from "react-router-dom";

class App extends Component {
	render() {
		return (
			<Fragment>
				<Header/>
				<Switch>
					<Route exact path="/" component={News}/>
					<Route  path="/news/:id" exact component={NewsOne}/>
					<Route exact path="/add_edit" component={Add}/>
				</Switch>
			</Fragment>
		);
	}
}

export default App;
