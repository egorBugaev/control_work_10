create  database `control_work_10`;
create table `control_work_10`.`news`(
  `id` int not null auto_increment primary key,
  `name` varchar(45) not null,
  `content` text,
  `image` text null,
  `date`  timestamp default current_timestamp not null
);
create table `control_work_10`.`comments`(
  `id` int not null auto_increment primary key,
  `news_id` int not null,
  `author` varchar(45) not null,
  `comment` text,
  index FK_news_idx (`news_id`),
  constraint FK_news
    foreign key (news_id)
    references control_work_10.news (id)
    on delete cascade
    on update cascade
);

