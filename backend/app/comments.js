const express = require('express');


const router = express.Router();

const createRouter = (db) => {

	router.get('/', (req, res) => {
		db.query('SELECT * FROM `news`', function (error, results) {
			if (error) throw error;
			res.send(results);
		});
	});
	

	router.post('/', (req, res) => {
		const comment = req.body;
		if (comment.author ==='') comment.author='Anonymous';
		db.query(
			'INSERT INTO `comments` (`author`, `comment`, `news_id`) ' +
			'VALUES (?, ?, ?)',
			[comment.author, comment.comment, comment.news_id],
			(error, results) => {
				if (error) throw error;
				
				comment.id = results.insertId;
				res.send(comment);
			}
		);
	});
	
	router.get('/news_id=:id', (req, res) => {
		console.log(req.params.id);
		db.query(`SELECT * FROM comments WHERE ID=${req.params.id}`, function (error, results) {
			if (error) throw error;
			res.send(results);
		});
	});
	
	router.delete('/:id', (req, res) => {
		db.query(`DELETE FROM comments WHERE id=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	return router;
};

module.exports = createRouter;