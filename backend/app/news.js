const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {

	router.get('/', (req, res) => {
		db.query('SELECT * FROM `news`', function (error, results) {
			if (error) throw error;
			res.send(results.map(item => {
				return {id: item.id, name: item.name, date: item.date, image: item.image}
			}));
		});
	});
	
	router.get('/:id', (req, res) => {
		let news=null;
		db.query(`SELECT * FROM news WHERE ID=${req.params.id}`, function (error, results) {
			if (error) throw error;
			news=results;
		});
		db.query(`SELECT * FROM comments WHERE news_id=${req.params.id}`, function (error, results) {
			if (error) throw error;
			news[0].comments=results;
			res.send(news);
		});
	});
	

	router.post('/', upload.single('image'), (req, res) => {
		const news = req.body;
		if (req.file) {
			news.image = req.file.filename;
		} else {
			news.image = null;
		}
	
		db.query(
			'INSERT INTO `news` (`name`, `content`, `image`) ' +
			'VALUES (?, ?, ?)',
			[news.name, news.content, news.image,],
			(error, results) => {
				if (error) {
					res.status(500);
					res.send(error);
				} else {
					news.id = results.insertId;
					res.send(news);
				}
			}
		);
	});
	
	router.delete('/:id', (req, res) => {
		db.query(`DELETE FROM news WHERE ID=${req.params.id}`, function (error, results){
		
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	
	return router;
};

module.exports = createRouter;